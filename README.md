# smqtt-springboot

#### 介绍
smqtt-spring-boot-starter demo程序

如选配了持久化模块
```xml
 		<dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>io.github.quickmsg</groupId>
            <artifactId>smqtt-persistent-db</artifactId>
            <version>1.1.1</version>
        </dependency>
```

那么需要添加文件： `resources/changelog/db.changelog-master.xml`

application.yml文件中添加配置：

```yaml
spring:
  liquibase:
    enabled: true
    change-log: classpath:/db/changelog/db.changelog-master.xml
```

