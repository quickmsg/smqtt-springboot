package com.smqtt.smqttspringboot;

import io.github.quickmsg.starter.EnableMqttServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableMqttServer
public class SmqttSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmqttSpringbootApplication.class, args);
    }

}
